package com.whispir.toyrobot;

import com.whispir.toyrobot.model.ToyRobot;
import com.whispir.toyrobot.service.CommandTranslator;
import com.whispir.toyrobot.service.ToyRobotProxy;
import com.whispir.toyrobot.utils.InputFileReader;

import java.util.List;

public class Main {

    public static void main(String args[]) {
        ToyRobotProxy toyRobotProxy = new ToyRobotProxy(new ToyRobot(), new CommandTranslator());

        List<String> commands = InputFileReader.read("input.txt");

        commands.forEach(toyRobotProxy::command);
    }
}
