package com.whispir.toyrobot.service;

import com.whispir.toyrobot.model.Direction;
import com.whispir.toyrobot.model.Position;
import com.whispir.toyrobot.model.Rotation;
import com.whispir.toyrobot.model.ToyRobot;

import java.util.function.Function;

public class CommandTranslator implements Function<String, Function<ToyRobot, ToyRobot>> {

    private static final String PLACE = "PLACE";

    private static final String MOVE = "MOVE";

    private static final String LEFT = "LEFT";

    private static final String RIGHT = "RIGHT";

    private static final String REPORT = "REPORT";

    @Override
    public Function<ToyRobot, ToyRobot> apply(String command) {
        if (MOVE.equals(command)) {
            return ToyRobot::move;
        } else if (LEFT.equals(command)) {
            return robot -> robot.rotate(Rotation.LEFT);
        } else if (RIGHT.equals(command)) {
            return robot -> robot.rotate(Rotation.RIGHT);
        } else if (REPORT.equals(command)) {
            return robot -> {
                robot.report();
                return robot;
            };
        } else if (isPlaceCommand(command)) {
            return place(command);
        } else {
            throw new IllegalArgumentException();
        }
    }

    public boolean isPlaceCommand(String command) {
        return command.startsWith(PLACE);
    }

    private Function<ToyRobot, ToyRobot> place(String command) {
        String[] arr = command.replaceAll("PLACE", "").trim().split(",");
        Position position = new Position(Integer.parseInt(arr[0]), Integer.parseInt(arr[1]));
        Direction direction = Direction.valueOf(arr[2]);

        return robot -> robot.place(position, direction);
    }
}
