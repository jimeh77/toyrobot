package com.whispir.toyrobot.service;

import com.whispir.toyrobot.model.ToyRobot;

public class ToyRobotProxy {

    private final CommandTranslator commandTranslator;

    private ToyRobot toyRobot;

    public ToyRobotProxy(ToyRobot toyRobot, CommandTranslator commandTranslator) {
        this.toyRobot = toyRobot;
        this.commandTranslator = commandTranslator;
    }

    public void command(String command) {
        if (!toyRobot.isPlaced() && !commandTranslator.isPlaceCommand(command)) {
            return;
        }

        applyCommand(command);
    }

    private void applyCommand(String command) {
        ToyRobot projectedToyRobot = commandTranslator.apply(command).apply(toyRobot);

        if (projectedToyRobot.getPosition().isValid()) {
            this.toyRobot = projectedToyRobot;
        }
    }

    public ToyRobot getToyRobot() {
        return toyRobot;
    }
}