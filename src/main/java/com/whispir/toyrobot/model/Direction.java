package com.whispir.toyrobot.model;

public enum Direction {

    NORTH, SOUTH, WEST, EAST

}
