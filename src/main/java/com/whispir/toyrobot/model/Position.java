package com.whispir.toyrobot.model;

public class Position {

    private final int x;

    private final int y;

    public Position(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int x() {
        return x;
    }

    public int y() {
        return y;
    }

    public boolean isValid() {
        return x >= 0 && x <= 5 && y >= 0 && y <= 5;
    }
}
