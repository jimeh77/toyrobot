package com.whispir.toyrobot.model;

public enum Rotation {
    LEFT, RIGHT
}
