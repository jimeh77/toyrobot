package com.whispir.toyrobot.model;

import static com.whispir.toyrobot.model.Direction.*;
import static com.whispir.toyrobot.model.Rotation.LEFT;
import static com.whispir.toyrobot.model.Rotation.RIGHT;

public class ToyRobot {

    private final Position position;

    private final Direction direction;

    public ToyRobot() {
        this(null, null);
    }

    public ToyRobot(Position position, Direction direction) {
        this.position = position;
        this.direction = direction;
    }

    public ToyRobot place(Position position, Direction direction) {
        return new ToyRobot(position, direction);
    }

    public ToyRobot move() {
        if (NORTH == direction) {
            return new ToyRobot(new Position(position.x(), position.y() + 1), direction);
        } else if (SOUTH == direction) {
            return new ToyRobot(new Position(position.x(), position.y() - 1), direction);
        } else if (WEST == direction) {
            return new ToyRobot(new Position(position.x() - 1, position.y()), direction);
        } else if (EAST == direction) {
            return new ToyRobot(new Position(position.x() + 1, position.y()), direction);
        } else {
            throw new IllegalStateException("Toy robot is facing unknown direction");
        }
    }

    public ToyRobot rotate(Rotation rotation) {
        if (LEFT == rotation) {
            return new ToyRobot(position, left());
        } else if (RIGHT == rotation) {
            return new ToyRobot(position, right());
        } else {
            throw new IllegalArgumentException("Toy robot is rotating to unknown");
        }
    }

    private Direction right() {
        if (NORTH == direction) {
            return EAST;
        } else if (SOUTH == direction) {
            return WEST;
        } else if (WEST == direction) {
            return NORTH;
        } else if (EAST == direction) {
            return SOUTH;
        } else {
            throw new IllegalStateException("Toy robot is facing unknown direction");
        }
    }

    private Direction left() {
        if (NORTH == direction) {
            return WEST;
        } else if (WEST == direction) {
            return SOUTH;
        } else if (SOUTH == direction) {
            return EAST;
        } else if (EAST == direction) {
            return NORTH;
        } else {
            throw new IllegalStateException("Toy robot is facing unknown direction");
        }
    }

    public boolean isPlaced() {
        return position != null && direction != null;
    }

    public void report() {
        String report = String.format("Output: %d,%d,%s", position.x(), position.y(), direction);
        System.out.println(report);
    }

    public Position getPosition() {
        return position;
    }

    public Direction getDirection() {
        return direction;
    }
}
