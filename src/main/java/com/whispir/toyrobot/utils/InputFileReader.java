package com.whispir.toyrobot.utils;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.List;

import static java.util.stream.Collectors.toList;

public class InputFileReader {

    public static List<String> read(String inputFile) {
        try {
            File file = new File(InputFileReader.class.getClassLoader().getResource(inputFile).getFile());

            return FileUtils.readLines(file, Charset.defaultCharset()).stream()
                    .map(String::trim)
                    .filter(str -> !str.isEmpty())
                    .collect(toList());
        } catch (IOException e) {
            throw new RuntimeException("Exception raised while reading input file", e);
        }
    }
}
