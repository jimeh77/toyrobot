package com.whispir.toyrobot.model;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static com.whispir.toyrobot.model.Direction.NORTH;
import static org.assertj.core.api.Assertions.assertThat;

public class ToyRobotTest {

    private final PrintStream originalOut = System.out;
    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private ToyRobot toyRobot;

    @Before
    public void setUp() {
        System.setOut(new PrintStream(outContent));

        toyRobot = new ToyRobot();
    }

    @After
    public void cleanUp() {
        System.setOut(originalOut);
    }

    @Test
    public void shouldPlace() {
        Position position = new Position(2, 3);

        ToyRobot actual = toyRobot.place(position, NORTH);

        assertThat(actual.getPosition()).isEqualTo(position);
        assertThat(actual.getDirection()).isEqualTo(NORTH);
    }

    @Test
    public void shouldReport() {
        toyRobot.place(new Position(1, 2), NORTH).report();

        assertThat(outContent.toString()).isEqualTo("Output: 1,2,NORTH\n");
    }

    @Test
    public void shouldIndicatePlaced() {
        ToyRobot actual = toyRobot.place(new Position(1, 2), NORTH);

        assertThat(actual.isPlaced()).isTrue();
    }

    @Test
    public void shouldIndicateNotPlaced() {
        assertThat(toyRobot.isPlaced()).isFalse();
    }
}