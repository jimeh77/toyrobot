package com.whispir.toyrobot.model;

import org.junit.Before;
import org.junit.Test;

import static com.whispir.toyrobot.model.Direction.*;
import static org.assertj.core.api.Assertions.assertThat;


public class ToyRobotMovementTest {

    private ToyRobot toyRobot;

    @Before
    public void setUp() {
        toyRobot = new ToyRobot();
    }

    @Test
    public void testMovement() {
        assertMovement(new Position(0, 0), NORTH, new Position(0, 1));
        assertMovement(new Position(0, 1), SOUTH, new Position(0, 0));
        assertMovement(new Position(1, 1), WEST, new Position(0, 1));
        assertMovement(new Position(1, 1), EAST, new Position(2, 1));
    }

    private void assertMovement(Position initial, Direction direction, Position expected) {
        ToyRobot actual = toyRobot.place(initial, direction).move();

        assertThat(actual.getPosition()).isEqualToComparingFieldByField(expected);
    }

}