package com.whispir.toyrobot.model;

import org.junit.Before;
import org.junit.Test;

import static com.whispir.toyrobot.model.Direction.*;
import static com.whispir.toyrobot.model.Rotation.LEFT;
import static com.whispir.toyrobot.model.Rotation.RIGHT;
import static org.assertj.core.api.Assertions.assertThat;


public class ToyRobotRotationTest {

    private ToyRobot toyRobot;

    @Before
    public void setUp() {
        toyRobot = new ToyRobot();
    }

    @Test
    public void testRotation() {
        assertRotation(NORTH, LEFT, WEST);
        assertRotation(SOUTH, LEFT, EAST);
        assertRotation(EAST, LEFT, NORTH);
        assertRotation(WEST, LEFT, SOUTH);

        assertRotation(NORTH, RIGHT, EAST);
        assertRotation(SOUTH, RIGHT, WEST);
        assertRotation(EAST, RIGHT, SOUTH);
        assertRotation(WEST, RIGHT, NORTH);
    }

    private void assertRotation(Direction initial, Rotation rotation, Direction expected) {
        ToyRobot actual = toyRobot.place(new Position(0, 0), initial).rotate(rotation);

        assertThat(actual.getDirection()).isEqualTo(expected);
    }
}