package com.whispir.toyrobot.model;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class PositionTest {

    @Test
    public void shouldIndicateValid() {
        assertThat(new Position(0, 0).isValid()).isTrue();
        assertThat(new Position(1, 1).isValid()).isTrue();
        assertThat(new Position(5, 5).isValid()).isTrue();
    }

    @Test
    public void shouldNotIndicateValid() {
        assertThat(new Position(-1, 0).isValid()).isFalse();
        assertThat(new Position(0, -1).isValid()).isFalse();
        assertThat(new Position(6, 5).isValid()).isFalse();
        assertThat(new Position(5, 6).isValid()).isFalse();
    }
}