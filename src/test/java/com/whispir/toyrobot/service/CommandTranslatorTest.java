package com.whispir.toyrobot.service;

import com.whispir.toyrobot.model.Position;
import com.whispir.toyrobot.model.Rotation;
import com.whispir.toyrobot.model.ToyRobot;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static com.whispir.toyrobot.model.Direction.WEST;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Matchers.eq;
import static org.mockito.Matchers.refEq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

@RunWith(MockitoJUnitRunner.class)
public class CommandTranslatorTest {

    private CommandTranslator commandTranslator = new CommandTranslator();

    @Mock
    private ToyRobot toyRobot;

    @Test
    public void shouldReturnPlaceCommand() {
        commandTranslator.apply("PLACE 5,1,WEST").apply(toyRobot);

        verify(toyRobot).place(refEq(new Position(5, 1)), eq(WEST));
        verifyNoMoreInteractions(toyRobot);
    }

    @Test
    public void shouldReturnMoveCommand() {
        commandTranslator.apply("MOVE").apply(toyRobot);

        verify(toyRobot).move();
        verifyNoMoreInteractions(toyRobot);
    }

    @Test
    public void shouldReturnRightCommand() {
        commandTranslator.apply("RIGHT").apply(toyRobot);

        verify(toyRobot).rotate(Rotation.RIGHT);
        verifyNoMoreInteractions(toyRobot);
    }

    @Test
    public void shouldReturnLeftCommand() {
        commandTranslator.apply("LEFT").apply(toyRobot);

        verify(toyRobot).rotate(Rotation.LEFT);
        verifyNoMoreInteractions(toyRobot);
    }

    @Test
    public void shouldReturnReportCommand() {
        assertThat(commandTranslator.apply("REPORT").apply(toyRobot)).isSameAs(toyRobot);

        verify(toyRobot).report();
        verifyNoMoreInteractions(toyRobot);
    }

    @Test
    public void shouldIndicatePlaceCommand() {
        assertThat(commandTranslator.isPlaceCommand("PLACE 5,1,WEST")).isTrue();
    }

    @Test
    public void shouldIndicateNotPlaceCommand() {
        assertThat(commandTranslator.isPlaceCommand("MOVE")).isFalse();
        assertThat(commandTranslator.isPlaceCommand("REPORT")).isFalse();
    }
}