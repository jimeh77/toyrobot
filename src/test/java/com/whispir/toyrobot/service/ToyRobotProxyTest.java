package com.whispir.toyrobot.service;

import com.whispir.toyrobot.model.Position;
import com.whispir.toyrobot.model.ToyRobot;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static com.whispir.toyrobot.model.Direction.NORTH;
import static com.whispir.toyrobot.model.Direction.WEST;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class ToyRobotProxyTest {

    private ToyRobotProxy service;

    @Mock
    private CommandTranslator commandTranslator;

    private String command = "command";

    @Mock
    private ToyRobot toyRobot;

    @Before
    public void setUp() {
        service = new ToyRobotProxy(toyRobot, commandTranslator);
    }

    @Test
    public void shouldIgnoreCommandWhenRobotNotPlaced() {
        service.command(command);

        verify(commandTranslator).isPlaceCommand(command);
        verify(toyRobot).isPlaced();
        verifyNoMoreInteractions(toyRobot, commandTranslator);

        assertThat(service.getToyRobot()).isSameAs(toyRobot);
    }

    @Test
    public void shouldIgnoreCommandWhenCausingInvalidPosition() {
        when(toyRobot.isPlaced()).thenReturn(true);
        when(commandTranslator.apply(command)).thenReturn((robot) -> new ToyRobot(new Position(6, 6), NORTH));

        service.command(command);

        assertThat(service.getToyRobot()).isSameAs(toyRobot);
    }

    @Test
    public void shouldApplyCommandWhenNotCausingInvalidPosition() {
        when(toyRobot.isPlaced()).thenReturn(true);
        ToyRobot newToyRobot = new ToyRobot(new Position(1, 1), WEST);
        when(commandTranslator.apply(command)).thenReturn((robot) -> newToyRobot);

        service.command(command);

        assertThat(service.getToyRobot()).isSameAs(newToyRobot);
    }
}