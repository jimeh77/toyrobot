package com.whispir.toyrobot;

import com.whispir.toyrobot.model.ToyRobot;
import com.whispir.toyrobot.service.CommandTranslator;
import com.whispir.toyrobot.service.ToyRobotProxy;
import com.whispir.toyrobot.utils.InputFileReader;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class ToyRobotComponentTest {

    private PrintStream originalOut = System.out;

    private ByteArrayOutputStream outContent = new ByteArrayOutputStream();

    private ToyRobotProxy toyRobotProxy = new ToyRobotProxy(new ToyRobot(), new CommandTranslator());

    @Before
    public void setUp() {
        System.setOut(new PrintStream(outContent));
    }

    @After
    public void cleanUp() {
        System.setOut(originalOut);
    }

    @Test
    public void test() {
        test("input_test_1.txt", "Output: 0,1,NORTH");
        test("input_test_2.txt", "Output: 0,0,WEST");
        test("input_test_3.txt", "Output: 3,3,NORTH");
    }

    private void test(String inputFile, String expectedReport) {
        List<String> commands = InputFileReader.read(inputFile);

        commands.forEach(toyRobotProxy::command);

        assertThat(outContent.toString()).isEqualTo(expectedReport + "\n");

        outContent.reset();
    }
}
