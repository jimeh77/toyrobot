Whispir Coding Challenge
=========

## Assumptions
- All input commands are valid

## How to Run
Run the Main class with the input.txt installed in the main resources directory.

Feel free to change the content of the file with different input commands.

